<%@ page import="com.tui.uk.config.ConfReader;"%>
<%@include file="/common/commonTagLibs.jspf"%>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<c:choose>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHOMSON'}">
			<title>Thomson | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
			<title>Falcon | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
			<title>First Choice | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHFO'}">
			<title>Flight Only | Error</title>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCTHCRUISE'}">
			<title>Cruise | Error</title>
		</c:when>
		<c:otherwise>
			<title>Thomson | Error</title>
		</c:otherwise>
	</c:choose>
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
    <c:choose>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON'}">
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/base.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/base-new-th.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/bf.css"/>
            <link rel="stylesheet" href="/cms-cps/amendandcancel/ancfalcon/css/ac.css"/>
            <%
			 	String getSummaryPageURL =(String)ConfReader.getConfEntry("ANCFALCON.summarypage.url","");
			    pageContext.setAttribute("getSummaryPageURL",getSummaryPageURL);	
			%>
		</c:when>
		<c:when test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFIRSTCHOICE'}">
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/base.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/base-new-fc.css"/>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/bf.css"/>
            <link rel="stylesheet" href="/cms-cps/amendandcancel/ancfirstchoice/css/ac.css"/>
            <%
			 	String getSummaryPageURL =(String)ConfReader.getConfEntry("ANCFIRSTCHOICE.summarypage.url","");
			    pageContext.setAttribute("getSummaryPageURL",getSummaryPageURL);	
			%>
		</c:when>
		<c:otherwise>
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/base.css" />
	 		<link rel="stylesheet" href="/cms-cps/amendandcancel/css/base-new-th.css" />
	        <link rel="stylesheet" href="/cms-cps/amendandcancel/css/bf.css" />
            <link rel="stylesheet" href="/cms-cps/amendandcancel/css/ac.css"/>
            <%
			 	String getSummaryPageURL =(String)ConfReader.getConfEntry("ANCTHOMSON.summarypage.url","");
			    pageContext.setAttribute("getSummaryPageURL",getSummaryPageURL);	
			%>
		</c:otherwise>
	</c:choose>
	
	<link rel="stylesheet" href="/cms-cps/amendandcancel/css/fonts.css"/>
	
	<c:choose>
		 <c:when test="${not empty bookingComponent.prePaymentUrl}">
		 	<c:set var="summaryPageURL" value="${bookingComponent.prePaymentUrl}"/>
	      </c:when>
	     <c:otherwise>
	        <c:set var="summaryPageURL" value="${getSummaryPageURL}"/>
	     </c:otherwise>
	</c:choose>	
	<script src="//nexus.ensighten.com/tui/Bootstrap.js"></script>
	<c:set var="analyticsPageID" value="technicaldifficulties"/>
</head>
<body>
	<div class="structure">
	
		<div id="page">
			<%
			     String analyticsPageID = (String)pageContext.getAttribute("analyticsPageID");
			     pageContext.setAttribute("analyticsPageID", analyticsPageID, PageContext.REQUEST_SCOPE);
			%>
			<jsp:include page="sprocket/header.jsp" />
			
			<div id="content" class="book-flow">
				<div class="content-width">
					
					<!-- Sorry - something has gone wrong... -->
					<div class="error-oh bg-light-grey marg-bottom-20">
						<div class="halfs">
							<div class="crop">
								<img src="/cms-cps/amendandcancel/images/658technical.jpg" srcset="
									/cms-cps/amendandcancel/images/658technical.jpg 658w,
									/cms-cps/amendandcancel/images/658technical.jpg 488w,
									/cms-cps/amendandcancel/images/658technical.jpg 232w" sizes="100vw" alt="" class="dis-block full" />
							</div>
						</div>					
						<div class="halfs copy">
							<h1>Technical difficulties.</h1>
							<p class="grey-med marg-bottom-10">We're really sorry we're having some technical problems. Try going <a href="<c:out value='${summaryPageURL}'/>">back to your booking summary page</a>.</p>
					        <p class="grey-med marg-bottom-10"></p>			
							
						</div>
					</div>
					
				</div>
			</div>
		
			<!--footer -->
			<jsp:include page="sprocket/footer.jsp" />
		
			<div class="page-mask"></div>
		</div>
		
	</div>
	<script src="/cms-cps/amendandcancel/js/iscroll-lite.js" type="text/javascript"></script>
<script>
	  var tui = {};
	  tui.analytics = {};
	  tui.analytics.page = {};
	  tui.analytics.page.pageUid = "technicaldifficultiespage";
	 </script>
</body>
</html>
