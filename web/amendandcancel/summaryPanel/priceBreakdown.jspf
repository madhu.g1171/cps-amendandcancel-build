<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>	
<fmt:formatNumber value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}" var="totalCostingLine" type="number" pattern="####" maxFractionDigits="2" minFractionDigits="2"/>
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<div class="breakdown">
	<c:set var="totalPaidDate" value="${bookingComponent.totalPaidTillDate.amount}" scope="page"/>
	<c:set var="priceBreakDown"
	            value="${bookingComponent.pricingDetails['priceBreakDown']}" />
	<c:set var="holidayExtras"
	                value="${bookingComponent.pricingDetails['HOLIDAY_EXTRAS_PRICE_COMPONENT']}" />
	<c:set var="flightExtras"
	                value="${bookingComponent.pricingDetails['FLIGHT_EXTRAS_PRICE_COMPONENT']}" />
	<c:set var="accomExtras"
					value="${bookingComponent.pricingDetails['ACCOM_EXTRAS_PRICE_COMPONENT']}" />                    
	<c:set var="cruiseExtras"
	                value="${bookingComponent.pricingDetails['CRUISE_EXTRAS_PRICE_COMPONENT']}" />
	<c:set var="optionsAndExtras" value="Options & Extras (total)"/>
	<c:if test="${not empty priceBreakDown}">
		<p class="title">PRICE BREAKDOWN</p>
		<ul class="grey-med">			
			<c:forEach var="priceComponent" items="${priceBreakDown}">
				<c:if test="${priceComponent.itemDescription != optionsAndExtras}">
					<li class="marg-bottom-5 text-wrap">
						<span class="text"><c:out value="${priceComponent.itemDescription}"/></span>
						<span class="abs right-0">
							<c:choose>
		                        <c:when
		                            test="${priceComponent.itemDescription == 'Online Discount'}">
		                          <span class="red"> -${poundOrEuro}<fmt:formatNumber
		                                    value="${priceComponent.amount.amount}" type="number"
		                                    maxFractionDigits="2" minFractionDigits="2"
		                                    pattern="#####.##" /></span>
		                        </c:when>
		                        <c:when
		                            test="${priceComponent.itemDescription == 'Promotional Discount'}">
		                            -${poundOrEuro}<fmt:formatNumber
		                                    value="${priceComponent.amount.amount}" type="number"
		                                    maxFractionDigits="2" minFractionDigits="2"
		                                    pattern="#####.##" />
		                        </c:when>
		                        <c:when
		                            test="${priceComponent.itemDescription == 'Basic Holiday'}">
		                            ${poundOrEuro}<fmt:formatNumber value="${priceComponent.amount.amount}" type="number"
		                                    maxFractionDigits="2" minFractionDigits="2"
		                                    pattern="#####.##" />
		                        </c:when>
		                        <c:when
		                            test="${priceComponent.itemDescription != 'Options & Extras (total)'}">
		                            ${poundOrEuro}<fmt:formatNumber value="${priceComponent.amount.amount}" type="number"
		                                    maxFractionDigits="2" minFractionDigits="2"
		                                    pattern="#####.##" />
		                        </c:when>
		                        <c:otherwise>
		                            +${poundOrEuro}<fmt:formatNumber value="${priceComponent.amount.amount}" type="number"
		                                    maxFractionDigits="2" minFractionDigits="2"
		                                    pattern="#####.##" />
		                        </c:otherwise>
		                    </c:choose>
						 </span>
					</li>
				</c:if>
			</c:forEach>			
		</ul>
	</c:if>
	<c:if test="${clientapp == 'ANCTHCRUISE'}">
		<c:if test="${not empty cruiseExtras}">
			<c:forEach var="cruiseExtra" items="${cruiseExtras}">
				<c:if test="${cruiseExtra.additionalPriceInfo != 'included'}">
					<li class="marg-bottom-5 text-wrap">
						<span class="text">
							<c:out value="${cruiseExtra.itemDescription}"/> 
			                <c:if test="${cruiseExtra.quantity ne null && cruiseExtra.quantity > 0 && !fn:containsIgnoreCase(cruiseExtra.itemDescription,'airport')}"> x <c:out
			                                         value="${cruiseExtra.quantity}" />
		                    </c:if>
	                    </span>
	                    <span class="abs right-0">
                          <c:if test="${cruiseExtra.amount.amount >= 0 && cruiseExtra.quantity ne null && cruiseExtra.quantity > 0}">
                          	  +${poundOrEuro}<fmt:formatNumber value="${cruiseExtra.amount.amount}" type="number"
                              maxFractionDigits="2" minFractionDigits="2"
                              pattern="#####.##" />
                          </c:if>
                          <c:if test="${cruiseExtra.amount.amount < 0}">
                              -${poundOrEuro}<fmt:formatNumber value="${cruiseExtra.amount.amount}"
							  type="number" maxFractionDigits="2" minFractionDigits="2"
							  pattern="#####.##" />
                          </c:if>
		                </span>
					</li>
				</c:if>
			</c:forEach>
		</c:if>
	</c:if>
	<c:if test="${not empty flightExtras}">
		<ul class="grey-med">
			<c:forEach var="flightExtra" items="${flightExtras}">
                <c:if test="${flightExtra.additionalPriceInfo != 'included'}">
	                <c:if test="${flightExtra.amount.amount > 0}">
		                <li class="marg-bottom-5 text-wrap">  
			                <span class="text">
				                <c:out value="${flightExtra.itemDescription}"/> 
				                <c:if test="${flightExtra.quantity ne null && flightExtra.quantity > 0 && !fn:containsIgnoreCase(flightExtra.itemDescription,'airport')}"> x <c:out
				                                         value="${flightExtra.quantity}" />
			                    </c:if>
		                    </span>
		                    <span class="abs right-0">
	                              +${poundOrEuro}<fmt:formatNumber value="${flightExtra.amount.amount}" type="number"
	                              maxFractionDigits="2" minFractionDigits="2"
	                              pattern="#####.##" />
			                </span>
		                </li>
		             </c:if>
	             </c:if>   
	         </c:forEach>
		 </ul>	
	</c:if>
	<c:if test="${clientapp != 'ANCTHFO'}">
		<c:if test="${not empty accomExtras}">
			<ul class="grey-med">
			   <c:forEach var="accomExtra" items="${accomExtras}">
					<c:if test="${accomExtra.additionalPriceInfo != 'included'}">
						<c:if test="${accomExtra.amount.amount >= 0 && accomExtra.quantity ne null && accomExtra.quantity > 0}">
							<li class="marg-bottom-5 text-wrap">
							    <span class="text">
								    <c:out value="${accomExtra.itemDescription}"
									escapeXml='false' /> <c:if
									test="${accomExtra.quantity ne null && accomExtra.quantity > 0 && !fn:containsIgnoreCase(accomExtra.itemDescription,'airport')}"> x <c:out
										value="${accomExtra.quantity}" />
								    </c:if> 
							    </span>
								<span class="abs right-0"> 	
									+${poundOrEuro}<fmt:formatNumber value="${accomExtra.amount.amount}" type="number" maxFractionDigits="2" minFractionDigits="2" pattern="#####.##" />
								</span>
							</li>
						</c:if>
					</c:if>
				</c:forEach>
			</ul>
		</c:if>
	</c:if>
	<c:if test="${not empty holidayExtras}">
		<ul class="grey-med">
			<c:forEach var="holidayExtra" items="${holidayExtras}">
				<c:if test="${holidayExtra.additionalPriceInfo != 'included'}">
					<c:if test="${holidayExtra.amount.amount > 0}">
						<li class="marg-bottom-5 text-wrap">
							<span class="text">
								<c:out value="${holidayExtra.itemDescription}"/>
								<c:if test="${holidayExtra.quantity ne null && holidayExtra.quantity > 0 && !fn:containsIgnoreCase(holidayExtra.itemDescription,'airport')}"> x <c:out
			                                        value="${holidayExtra.quantity}" />
			                    </c:if>
		                    </span> 
							<span class="abs right-0">
                                +${poundOrEuro}<fmt:formatNumber value="${holidayExtra.amount.amount}" type="number"
                                    maxFractionDigits="2" minFractionDigits="2"
                                    pattern="#####.##" />
							</span>
						</li>
					</c:if>
				</c:if>
			</c:forEach>
		</ul>	
	</c:if>				
	<ul class="grey-med">
		<c:if test="${(not empty bookingComponent.creditCardFee) && (not empty bookingComponent.creditCardFee.amount)}">
		   <li class="marg-bottom-5">Credit Card Fee<span>+${poundOrEuro}<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" 
												value="${bookingComponent.creditCardFee.amount}" pattern="#####.##"/></span></li>
		</c:if>
		<li class="marg-bottom-5">Total paid to date<span>+${poundOrEuro}<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" 
	                                  value="${totalPaidDate}" pattern="#####.##"/></span></li>
	</ul>
</div>