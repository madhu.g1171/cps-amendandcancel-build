<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:formatNumber
	value="${bookingComponent.totalAmount.amount-bookingInfo.calculatedDiscount.amount}"
	var="totalCostingLine" type="number" pattern="####"
	maxFractionDigits="2" minFractionDigits="2" />
<c:set value="${totalCostingLine}" var="totalCostingLine" scope="page"/>
<c:set var="totalcost" value="${fn:split(totalCostingLine, '.')}" />
<c:if test="${not empty bookingComponent.breadCrumbTrail}">
	<c:set var="optionsUrl"
		value="${bookingComponent.breadCrumbTrail['HOTEL']}" />
	<c:set var="searchUrl"
		value="${bookingComponent.breadCrumbTrail['HUBSUMMARY']}" />
	<c:set var="passengersUrl"
		value="${bookingComponent.breadCrumbTrail['PASSENGERS']}" />
</c:if>

<c:choose>
	<c:when
		test="${bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFALCON' || (bookingInfo.bookingComponent.clientApplication.clientApplicationName == 'ANCFJFO')}">
		<c:set var="poundOrEuro" value="&euro;"/>
	</c:when>
	<c:otherwise>
		<c:set var="poundOrEuro" value="&pound;"/>
	</c:otherwise>
</c:choose>

<c:choose>
   <c:when test="${bookingComponent.nonPaymentData['TUIHeaderSwitch'] eq 'ON'}">
      <c:set value="true" var="tuiLogo" />
   </c:when>
   <c:otherwise>
      <c:set value="false" var="tuiLogo" />  
   </c:otherwise>
</c:choose>

<div id="header-nav" class="c">
	<div class="content-width">
   <div id="logo-section">    
			<div id="logo<c:if test="${tuiLogo}">TUI</c:if>">
				<a href="${bookingComponent.clientURLLinks.homePageURL}"></a>
			</div>
	  <div class="summary-panel-trigger b abs bg-tui-sand black">
				<i class="caret fly-out"></i>${poundOrEuro}<c:out
					value="${totalcost[0]}." />
				<span class="pennys"><c:out value="${totalcost[1]}" /></span>
	  </div>
   </div>
		<c:if test="${!tuiLogo}">
		<img alt="World Of TUI" data-mob-position="0" data-tab-position="1"
			data-minitab-position="0" data-desktop-position="3"
			class="nomobile nominitablet marg-all-0-imp" id="header-wtui"
			src="https://digital.thomson.co.uk/wot.png"> <span
			class="shadow"></span></c:if>
	</div>
</div>