/*
 * Copyright (C)2013 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 * 
 * $RCSfile: $
 * 
 * $Revision: $
 * 
 * $Date: $
 * 
 * $Author: $
 * 
 * $Log: $
 */
package com.tui.uk.payment.processor;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.Map;

import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;

/**
 * This performs all the operations required for post payment for Mobile Flight Only application.
 *
 * @author atul.rao
 *
 */
public class ANCPostPaymentProcessor extends ThomsonPostPaymentProcessor
{
   // CHECKSTYLE:OFF
   /** The terms and condition checkbox. */
   private static final String TERMS_AND_CONDITION = "agreeTermAndCondition";

   /** The deposit type */
   private static final String DEPOSIT = "depositType";

   /**
    * Constructor with Payment Data.
    *
    * @param paymentData the payment data.
    * @param requestParameterMap the request parameter map.
    */
   public ANCPostPaymentProcessor(PaymentData paymentData, 
                                  Map<String, String[]> requestParameterMap)
   {
      super(paymentData, requestParameterMap);
   }

   /**
    * This method is responsible for pre process of the data entered in the payment page. This may
    * include, updating non payment data map, validation.
    *
    * @throws PostPaymentProcessorException if validation fails.
    */
   public void preProcess() throws PostPaymentProcessorException
   {
      if (!bookingComponent.getClientApplication().getClientApplicationName().equals("ANCFALCON") || !bookingComponent.getClientApplication().getClientApplicationName().equals("ANCFJFO"))
      {
         validateGiftCard();
      }

      ANCValidationErrors errors = new ANCValidationErrors();
      ANCFalconValidationErrors ancFalconErrors = new ANCFalconValidationErrors();

      BigDecimal fullcost = bookingComponent.getPayableAmount().getAmount();
      Currency GBP = Currency.getInstance(Locale.UK);
      Currency EUR = Currency.getInstance("EUR");

      if (!bookingComponent.getClientApplication().getClientApplicationName().equals("ANCTHFO"))
      {
         if(requestParameterMap.get(DEPOSIT) != null)
         {
            if (requestParameterMap.get(DEPOSIT).equalsIgnoreCase("PART_PAYMENT"))
            {
               for (DepositComponent depositComponent : bookingComponent.getDepositComponents())
               {
                  if (depositComponent.getDepositType().equalsIgnoreCase("PART_PAYMENT"))
                  {
                     BigDecimal PART_PAYMENT = new BigDecimal(requestParameterMap.get("part"));

                     if (bookingComponent.getClientApplication().getClientApplicationName()
                        .equals("ANCFALCON") || bookingComponent.getClientApplication().getClientApplicationName().equals("ANCFJFO"))
                     {
                        depositComponent.setDepositAmount(new Money(PART_PAYMENT, EUR));
                     }
                     else
                     {
                        depositComponent.setDepositAmount(new Money(PART_PAYMENT, GBP));
                     }
                     if (PART_PAYMENT.compareTo(BigDecimal.ZERO) == -1)
                     {
                        throw new PostPaymentProcessorException(
                           "nonpaymentdatavalidation.amendandcancel.amount");
                     }
                     if (fullcost.compareTo(PART_PAYMENT) == -1)
                     {
                        throw new PostPaymentProcessorException(
                           "nonpaymentdatavalidation.amendandcancel.amount");
                     }
                  }
               }
            }
         }
      }

      if (requestParameterMap.get(TERMS_AND_CONDITION) == null)
      {
         String errorMessage =
            PropertyResource
               .getProperty("booking.termsandconditions.notchecked", MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(errorMessage);
         throw new PostPaymentProcessorException("booking.termsandconditions.notchecked");
      }

      if (bookingComponent.getClientApplication().getClientApplicationName().equals("ANCFALCON"))
      {
         if (!(ancFalconErrors.patternCheck("firstName", requestParameterMap.get("firstName"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.amendandcancel.firstName");
         }

         if (!(ancFalconErrors.patternCheck("surName", requestParameterMap.get("surName"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.amendandcancel.surName");
         }

         if (!(ancFalconErrors.patternCheck("streetAddress1",
            requestParameterMap.get("payment_0_street_address1"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.amendandcancel.streetAddress1");
         }

         if (requestParameterMap.get("payment_0_street_address2") != "")
         {
            if (!(ancFalconErrors.patternCheck("streetAddress2",
               requestParameterMap.get("payment_0_street_address2"))))
            {
               throw new PostPaymentProcessorException(
                  "nonpaymentdatavalidation.amendandcancel.streetAddress2");
            }
         }
         if (requestParameterMap.get("payment_0_selectedCountry").equals("IE"))
         {
            if (!(ancFalconErrors.patternCheck("town_republic",
               requestParameterMap.get("payment_0_street_address3"))))
            {
               throw new PostPaymentProcessorException(
                  "nonpaymentdatavalidation.amendandcancel.town");
            }

           /* if (!(ancFalconErrors.patternCheck("county_republic",
               requestParameterMap.get("payment_0_street_address4"))))
            {
               throw new PostPaymentProcessorException(
                  "nonpaymentdatavalidation.amendandcancel.county");
            }
*/
         }
         else if (requestParameterMap.get("payment_0_selectedCountry").equals("GB"))
         {
            if (!(ancFalconErrors.patternCheck("town_ireland",
               requestParameterMap.get("payment_0_street_address3"))))
            {
               throw new PostPaymentProcessorException(
                  "nonpaymentdatavalidation.amendandcancel.town");
            }
          /*  if (!(ancFalconErrors.patternCheck("county_ireland",
               requestParameterMap.get("payment_0_street_address4"))))
            {
               throw new PostPaymentProcessorException(
                  "nonpaymentdatavalidation.amendandcancel.county");
            }*/
         }
         if (requestParameterMap.get("payment_0_selectedCountry").equals("IE"))
         {
            if (requestParameterMap.get("payment_0_postCode") != null)
            {
               if (!requestParameterMap.get("payment_0_postCode").trim().equals(""))
               {
                  if (!(ancFalconErrors.patternCheck("postcode_republic",
                     requestParameterMap.get("payment_0_postCode"))))
                  {
                     throw new PostPaymentProcessorException(
                        "nonpaymentdatavalidation.invalid.postCode");
                  }
               }
            }
         }
         else if (requestParameterMap.get("payment_0_selectedCountry").equals("GB"))
         {
            if (!(ancFalconErrors.patternCheck("postcode_ireland",
               requestParameterMap.get("payment_0_postCode"))))
            {
               throw new PostPaymentProcessorException
                             ("nonpaymentdatavalidation.invalid.postCode");
            }
         }
      }
      else
      {
         if (!(errors.patternCheck("firstName", requestParameterMap.get("firstName"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.amendandcancel.firstName");
         }

         if (!(errors.patternCheck("surName", requestParameterMap.get("surName"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.amendandcancel.surName");
         }

         if (!(errors.patternCheck("streetAddress1",
            requestParameterMap.get("payment_0_street_address1"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.amendandcancel.streetAddress1");
         }

         if (requestParameterMap.get("payment_0_street_address2") != "")
         {
            if (!(errors.patternCheck("streetAddress2",
               requestParameterMap.get("payment_0_street_address2"))))
            {
               throw new PostPaymentProcessorException(
                  "nonpaymentdatavalidation.amendandcancel.streetAddress2");
            }
         }
         if (!(errors.patternCheck("town", requestParameterMap.get("payment_0_street_address3"))))
         {
            throw new PostPaymentProcessorException
                          ("nonpaymentdatavalidation.amendandcancel.town");
         }
      /*   if (!(errors.patternCheck("county",
                                   requestParameterMap.get("payment_0_street_address4"))))
         {
            throw new PostPaymentProcessorException(
               "nonpaymentdatavalidation.amendandcancel.county");
         }*/

         if (bookingComponent.getClientApplication().getClientApplicationName().equals("ANCTHFO"))
         {
            if (requestParameterMap.get("payment_0_selectedCountry") == "GB")
            {
               if (!(errors.patternCheck("postcode", 
                                          requestParameterMap.get("payment_0_postCode"))))
               {
                  throw new PostPaymentProcessorException(
                     "nonpaymentdatavalidation.amendandcancel.postCode");
               }
            }
         }
         else
         {
            if (!(errors.patternCheck("postcode", requestParameterMap.get("payment_0_postCode"))))
            {
               throw new PostPaymentProcessorException(
                  "nonpaymentdatavalidation.amendandcancel.postCode");
            }
         }
      }

      if (requestParameterMap.get("payment_0_selectedCountryCode") == "")
      {
         throw new PostPaymentProcessorException
                       ("nonpaymentdatavalidation.amendandcancel.country");
      }

      if (requestParameterMap.get("title") == "")
      {
         throw new PostPaymentProcessorException("nonpaymentdatavalidation.amendandcancel.title");
      }
      
	  if (!bookingComponent.getClientApplication().getClientApplicationName().equals("ANCFALCON") || !bookingComponent.getClientApplication().getClientApplicationName().equals("ANCFJFO"))
      {
		  if(requestParameterMap.get("payment_0_type") != null
			  && requestParameterMap.get("payment_0_type").contains("TUI_MASTERCARD"))
		  {
			  String thCCBinRange = "ThomsonCreditcard.BINRange";
			  String thCCNumberSelected = null;
			  String thCCConfiguration = ConfReader.getConfEntry(thCCBinRange, null);
			  String cardNumber = requestParameterMap.get("payment_0_cardNumber");
			  String[] thCCNumberList = thCCConfiguration.split(",");

			  for (String thCCNumber : thCCNumberList)
			  {
				   if (cardNumber.startsWith(thCCNumber))
				   {
						 thCCNumberSelected = thCCNumber;
						 break;
				   }
			  }
			  if (thCCNumberSelected != null)
			  {
				   if (!(requestParameterMap.get("payment_0_type").contains("TUI_MASTERCARD")))
				   {
						 throw new PostPaymentProcessorException("datacash.cardtype.mismatch");
				   }
			  }
          }
	  }
      super.preProcess();
   }

   /**
    * This method is responsible for creating transactions and updating the payment data.
    *
    * @throws PaymentValidationException if validation fails while creating transactions.
    * @throws PostPaymentProcessorException if the amount paid and the payable amount do not match.
    */
   public void process() throws PaymentValidationException, PostPaymentProcessorException
   {
      Money amendmentChargeMoney = bookingComponent.getAmendmentCharge();
      BigDecimal amendmentChargeBD = null;
      if (amendmentChargeMoney != null)
      {
         amendmentChargeBD = amendmentChargeMoney.getAmount();
      }

      if (requestParameterMap.get(BookingConstants.DEPOSIT_TYPE) != null)
      {
         String depositType = requestParameterMap.get(BookingConstants.DEPOSIT_TYPE).trim();
         if (depositType != null && depositType.equalsIgnoreCase("fullcost"))
         {
            if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
            {
               if (bookingComponent.getPayableAmount() != null)
               {
                  Money totalWithAmendmentCharge =
                     amendmentChargeMoney.add(bookingComponent.getPayableAmount());
                  bookingInfo.setCalculatedTotalAmount(totalWithAmendmentCharge);
                  bookingInfo.setCalculatedPayableAmount(totalWithAmendmentCharge);
               }
               else
               {
                  bookingInfo.setCalculatedTotalAmount(amendmentChargeMoney);
                  bookingInfo.setCalculatedPayableAmount(amendmentChargeMoney);
               }
            }
            else
            {
               bookingInfo.setCalculatedTotalAmount(bookingComponent.getPayableAmount());
            }
         }
         else if (depositType != null && depositType.equalsIgnoreCase("onlyAmendCharge"))
         {
            if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
            {
               bookingInfo.setCalculatedTotalAmount(amendmentChargeMoney);
               bookingInfo.setCalculatedPayableAmount(amendmentChargeMoney);
            }
         }
      }
      else
      {
         if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
         {
            bookingInfo.setCalculatedTotalAmount(amendmentChargeMoney);
            bookingInfo.setCalculatedPayableAmount(amendmentChargeMoney);
         } else{

        	 if(bookingComponent.getPayableAmount() != null && bookingComponent.getPayableAmount().getAmount().doubleValue()>0.00 ){
        		 bookingInfo.setCalculatedTotalAmount(bookingComponent.getPayableAmount());
                 bookingInfo.setCalculatedPayableAmount(bookingComponent.getPayableAmount());

        	 }
         }
      }
      super.process();
   }

}
